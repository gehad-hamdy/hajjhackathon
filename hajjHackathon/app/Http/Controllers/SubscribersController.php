<?php
/**
 * Created by PhpStorm.
 * User: Gehad
 * Date: 8/1/2018
 * Time: 11:18 PM
 */

namespace App\Http\Controllers;


use App\Http\Controllers\DB\BigQuery;
use App\Http\Controllers\DB\firebaseDB;

class SubscribersController extends Controller
{
    /**
     *
     */
    public function getUsers()
    {
//        $checkDB = new BigQuery();
//
//        return $checkDB->checkConnection();

        $c = new firebaseDB();

        return $c->configureFireBase();
    }

    public function createUser()
    {

    }
}