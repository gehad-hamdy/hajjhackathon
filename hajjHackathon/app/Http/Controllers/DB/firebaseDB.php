<?php
/**
 * Created by PhpStorm.
 * User: Gehad
 * Date: 8/2/2018
 * Time: 11:17 PM
 */

namespace App\Http\Controllers\DB;

use Kreait\Firebase\Database\Query;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

require 'C:\xampp\htdocs\hajjhackathon\hajjHackathon\vendor/autoload.php';

class firebaseDB
{
    /**
     * Configure connection to firebase
     * @return \Kreait\Firebase\Database
     */
    function configureFireBase()
    {
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__ . '/Pay4Me-67dc0ed89dfb.json');
        $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->withDatabaseUri('https://pay4me-212016.firebaseio.com/')
            ->create();


//        $discoverer = new ServiceAccount\Discoverer([
//            function () {
//                $serviceAccount = 'gfvgfie2afdihgfsxqgzb5yqym@speckle-umbrella-26.iam.gserviceaccount.com'; // Instance of Kreait\Firebase\ServiceAccount
//                return $serviceAccount;
//            }
//        ]);

        $database = $firebase->getDatabase();

//        $reference = $database
//            ->getReference('web4me/subscribers')
//            ->push([
//                'firstname ' => 'gehad',
//                'lastname ' => 'hamdy'
////                'email' => 'gh@shopbox.com',
////                'passport_number ' => 'A4586cnkjn',
////                'age' => 25,
////                'gender' => 'femail',
////                'mobile ' => '21589921455',
////                'package_id' => 1,
////                'points ' => 1500
//            ]);

        $reference = $database->getReference('https://pay4me-212016.firebaseio.com/subscribers')->set([
            'firstname ' => 'gehad',
            'lastname ' => 'hamdy'
        ]);

        return $reference->getValue();
    }

    /**
     * Echoing json response to client
     * @param String $status_code Http response code
     * @param Int $response Json response
     */
    function echoResponse($status_code, $response)
    {
        $app = \Slim\Slim::getInstance();
        // Http response code
        $app->status($status_code);
        // setting response content type to json
        $app->contentType('application/json');
        echo json_encode($response);
    }

    /**
     * Get user details by user id
     * @param $userId
     */
    function getUserById($userId)
    {
        $firebase = configureFireBase();
        $query = new Query();
        $query->orderByChildKey('user_id')->equalTo((int)$userId)->limitToFirst(1);
        $nodeGetContent = $firebase->users->query($query);
        if (count($nodeGetContent) > 0) {
            $nodeGetContent = array_values($nodeGetContent)[0];
            return $nodeGetContent;
        } else {
            return false;
        }
    }


}